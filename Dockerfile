FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG VERSION=1.14.0
ENV NEXT_TELEMETRY_DISABLED 1

RUN mkdir -p /app/code
WORKDIR /app/code

RUN curl -L https://github.com/enricoros/big-AGI/archive/refs/tags/v${VERSION}.zip -o /tmp/tmp.zip && unzip /tmp/tmp.zip -d /tmp && rm /tmp/tmp.zip
RUN mv /tmp/big-AGI-${VERSION}/* /app/code/

ENV NODE_ENV development
RUN npm ci
ENV NODE_ENV production
RUN npm run build
RUN npm prune --production
RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs
ENV PATH $PATH:/app/code/node_modules/.bin

USER nextjs

EXPOSE 3000

CMD ["next", "start"]
