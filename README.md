# cloudron-app-package
Package for deloyment of this app on Cloudron

## THIS IS ONLY A PROJECT TO MAKE IT POSSIBLE TO DEPLOY THIS APP ON CLOUDRON 

To deploy it manually on Cloudron follow this instruction:

You need to clone this repo to a local linux where the [Cloudron CLI](https://docs.cloudron.io/packaging/cli/) and Docker is installed. On your cloudron you can install the [Docker Registry App](https://docs.cloudron.io/apps/docker-registry/) to host the custom Image. In Cloudron itself you need to [configure your custom Docker Registry](https://docs.cloudron.io/settings/#private-docker-registry).

Build the docker Image and give it a Name:
~~~
docker build -t APPNAME .
~~~

Tag the docker Image and specify the URL of the Docker Registry that you will push it to (you may install the Docker Registry App in Cloudron):
~~~
docker tag APPNAME:latest URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Push Image to your Docker Registry:
~~~
docker push URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Install image on your Cloudron Instance with the URL of the Docker Registry:
~~~
cloudron install --image URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

# Easy Installation
Be aware that you will use a precreated Docker Image this way - you have to trust the Image Provider if you use this way to install

You will need to have a *read and write* Cloudron API Token. See [Documentation how to create one](https://docs.cloudron.io/profile/#api-tokens).

1) Install a new temporary Surfer App on Cloudron
2) Open Terminal of Surfer App
3) Enter ```cd /app/data```
4) Enter ```wget https://git.cloudron.io/kubernetes/cloudron-big-agi/-/archive/v0.0.1/cloudron-big-agi-v0.0.1.zip```
5) Enter ```unzip cloudron-big-agi-v0.0.1.zip && cd cloudron-big-agi-v0.0.1```
6) Edit the config.ini with File Editor in File Manager and enter the Cloudron API Acess Token, the Cloudron Instance Hostname and the desired App Location (Subdomain)
7) Go back to the Terminal and enter ```./easy-install.sh```
8) The installation take a couple of minutes and will tell you if it was successfull
9) The temporary Surfer App can be uninstalled now as it is not needed for running the App. If you reused and existing Surfer App, you can skip this step.
10) The installed package will now be visible in the Cloudron App panel. If it shows your Cloudron login, you can login with the cloudron credentials.

**Important: Please do not download the Archive to any other place than /app/data. If you put it in public/ you risk leakage of the Cloudron API Token you put in the config.ini file!**

# Warning: Not Production Ready

Please note that this project is not intended for production use. It is only a project to make it possible to deploy this app on Cloudron. Deploying this app manually on Cloudron requires advanced knowledge of Linux, the Cloudron CLI, and Docker.

Using a precreated Docker Image from an external source for easy installation comes with risks. You need to trust the Image Provider if you choose this installation method.

Before proceeding with the installation, please ensure that you understand the potential risks involved and take appropriate precautions to protect your system and data.

By continuing with the installation, you acknowledge and accept full responsibility for any consequences that may arise from using this project.

## License
This project is under the [GNU GPLv3](LICENSE).

